import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {CacheService} from "@app/services/cache.service";
import {tap} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class CachingInterceptorService implements HttpInterceptor {
    constructor(private cache: CacheService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // т.к. необходимо кэшировать только GET запросы,
        // добавляем проверку на метод и наличие заголовка
        if (req.method === "GET" && req.headers.has("x-user-cache")) {
            let cachedData = this.cache.get(req.url);
            return cachedData ? of(new HttpResponse({body: JSON.parse(cachedData)})) : this.sendRequest(req, next)
        } else {
            return next.handle(req);
        }
    }

    private sendRequest(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>  {
        return next.handle(req).pipe(
            tap(event => {
                // Кроме HttpResponse может возникнуть другой ивент
                if (event instanceof HttpResponse) {
                    this.cache.put(req.url, JSON.stringify(event.body)); // кладём ивент в кэш
                }
            })
        );
    }
}
