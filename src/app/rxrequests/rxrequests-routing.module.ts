import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DatafetcherComponent } from "@app/rxrequests/components/datafetcher/datafetcher.component";
import { DatafetcherResolverService } from "@app/rxrequests/services/datafetcher-resolver.service";
import { RepeatUntilArrayNotEmptyComponent } from "@app/rxrequests/components/repeat-until-array-not-empty/repeat-until-array-not-empty.component";
import { MergeRequestsComponent } from "@app/rxrequests/components/merge-requests/merge-requests.component";
import { MergeRequestsResolverService } from "@app/rxrequests/services/merge-requests-resolver.service";

const routes: Routes = [
    { path: '', children: [
            { path: 'repeat', component: RepeatUntilArrayNotEmptyComponent, resolve: { resolved: DatafetcherResolverService } },
            { path: 'merge', component: MergeRequestsComponent, resolve: { resolved: MergeRequestsResolverService } }
        ], component: DatafetcherComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RxrequestsRoutingModule { }
