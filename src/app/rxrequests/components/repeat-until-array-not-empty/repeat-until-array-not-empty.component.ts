import { Component, OnInit } from '@angular/core';
import { DatafetcherService } from "@app/rxrequests/services/datafetcher.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-repeat-until-array-not-empty',
  templateUrl: './repeat-until-array-not-empty.component.html',
  styleUrls: ['./repeat-until-array-not-empty.component.scss']
})
export class RepeatUntilArrayNotEmptyComponent implements OnInit {
  public data: any;
  public error: string;

  constructor(private service: DatafetcherService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(resolved =>
        resolved.resolved.hasOwnProperty('error') ?
            this.error = resolved.resolved.error :
            this.data = resolved.resolved
    )
  }
}

