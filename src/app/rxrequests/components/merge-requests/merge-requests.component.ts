import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-merge-requests',
  templateUrl: './merge-requests.component.html',
  styleUrls: ['./merge-requests.component.scss']
})
export class MergeRequestsComponent implements OnInit {
  public data: any;
  public error: { detail: string };

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(resolved =>
        resolved.resolved.hasOwnProperty('error') ?
            this.error = resolved.resolved.error :
            this.data = resolved.resolved
    )
  }
}
