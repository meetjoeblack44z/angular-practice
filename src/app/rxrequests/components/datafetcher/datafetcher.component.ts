import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'app-datafetcher',
  templateUrl: './datafetcher.component.html',
  styleUrls: ['./datafetcher.component.scss']
})
export class DatafetcherComponent implements OnInit {
  private title = "DataFetcher component";
  public form: FormGroup;

  constructor(private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.form = this.fb.group({ id: ['', [Validators.required]] })
  }

  public submitForm() {
    this.router.navigateByUrl('datafetcher-component/merge', { state: { id: this.form.value.id } });
  }
}
