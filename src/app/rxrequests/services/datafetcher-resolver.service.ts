import { Injectable } from '@angular/core';
import { Resolve } from "@angular/router";
import {Observable, of, throwError} from "rxjs";
import { DatafetcherService } from "@app/rxrequests/services/datafetcher.service";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class DatafetcherResolverService implements Resolve<any>{

  constructor(private service: DatafetcherService) { }

  public resolve(): Observable<any> {
    return this.service.fetchData().pipe(catchError(err => of(err)))
  }
}
