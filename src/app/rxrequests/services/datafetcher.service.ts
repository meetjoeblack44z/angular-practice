import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError, delay, map, mergeMap, retryWhen, scan } from "rxjs/operators";
import { Person } from "@app/models/Person";
import { Planet } from "@app/models/Planet";

interface RetryArgs {
    retriesCount: number; // количество повторных попыток
    delayFor?: number; // задержка между попытками в миллисекундах
    altEndpoint?: string; // альтернативый URL
}

export const retryStrategy = (params: RetryArgs) => errors => {
    return errors.pipe(scan((retryNum, error) => {
        if (retryNum >= params.retriesCount) {
            throw error;
        }
        return ++retryNum;
    }, 0)).pipe(delay(params.delayFor));
};

@Injectable({
    providedIn: 'root'
})
export class DatafetcherService {
    private example_1: string = "https://swapi.co/api/people/2";
    private example_2: string = "https://swapi.co/api/people";

    constructor(private http: HttpClient) {
    }

    // в этом примере мы проверяем что массив starships в полученном JSON не пуст
    // если он пуст - повторяем запрос еще раз, но при третьей попытке
    // меняем endpoint на тот, который удовлетворяет условиям
    public fetchData(): Observable<Person> {
        return this.http.get<Person>(this.example_1).pipe(map(data => {
            if (data.starships.length) {
                return data
            } else {
                throw {error: 'no starships found!'}
            }
        }), retryWhen(retryStrategy({retriesCount: 2, delayFor: 3000})));
    }

    // в этом примере для загрузки данных компонента будет выполнено два запроса,
    // при этом во втором запросе будут использоваться данные из первого
    public mergeRequests(id: number): Observable<Planet> {
        console.log(`Trying to GET: ${this.example_2}/${id}`);
        return this.http.get(`${this.example_2}/${id}`)
            .pipe(mergeMap((data: Person) => {
                console.log(`Trying to GET homeworld: ${data.homeworld}`);
                return this.http.get(data.homeworld);
            }), catchError((err) => of(err)));
    }
}
