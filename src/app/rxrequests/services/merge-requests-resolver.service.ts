import { Injectable } from '@angular/core';
import { Router, Resolve } from "@angular/router";
import { DatafetcherService } from "@app/rxrequests/services/datafetcher.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MergeRequestsResolverService implements Resolve<any> {

  constructor(private service: DatafetcherService, private router: Router) { }

  public resolve(): Observable<any> {
    const state = this.router.getCurrentNavigation().extras.state;
    const id = (state === undefined || state.id === undefined || state.id === '') ? 1 : state.id;
    console.log(`resolved id: ${id}`);
    return this.service.mergeRequests(id);
  }
}
