import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { RxrequestsRoutingModule } from "@app/rxrequests/rxrequests-routing.module";
import { DatafetcherComponent } from '@app/rxrequests/components/datafetcher/datafetcher.component';
import { RepeatUntilArrayNotEmptyComponent } from '@app/rxrequests/components/repeat-until-array-not-empty/repeat-until-array-not-empty.component';
import { MatButtonModule } from "@angular/material/button";
import { MatGridListModule } from "@angular/material/grid-list";
import { MergeRequestsComponent } from '@app/rxrequests/components/merge-requests/merge-requests.component';
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";


@NgModule({
  declarations: [DatafetcherComponent, RepeatUntilArrayNotEmptyComponent, MergeRequestsComponent],
  imports: [
    CommonModule,
    RxrequestsRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class RxrequestsModule { }
