import { Component, OnInit } from '@angular/core';
import { AuthService } from "@app/services/auth.service";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  public title: string;

  constructor(public authService: AuthService) { }

  ngOnInit() {
    this.title = "navigation component"
  }

  public logout(): void {
    this.authService.logout();
  }
}
