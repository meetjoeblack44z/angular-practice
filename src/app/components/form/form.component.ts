import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  private title: string = "form component";
  public form: FormGroup;

  public submitted: string;

  private email: string;
  private deposit: number;

  // объект с текстом ошибок
  public fieldMessages = {
    email: {
      hint: "Мы отправим электронный чек",
      errors: {
        required: "",
        pattern: "Почта должна быть в формате example@domain.com"
      }
    },
    deposit: {
      hint: "Максимальная сумма депозита – $500",
      errors: {
        required: "",
        max: "Максимальный депозит составляет $500",
        min: "Минимальный депозит составляет $1"
      }
    }
  };

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      email: [this.email, [
        Validators.required,
        Validators.pattern("[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}")
      ]],
      deposit: [this.deposit, [
        Validators.required,
        Validators.min(1),
        Validators.max(500)
      ]]
    });
  }

  public submitForm() { this.submitted = JSON.stringify(this.form.value); }
}
