import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content-dummy',
  templateUrl: './content-dummy.component.html',
  styleUrls: ['./content-dummy.component.scss']
})
export class ContentDummyComponent implements OnInit {
  private title: string = "homepage";

  constructor() { }

  ngOnInit() {
  }

}
