import { Component, Input, OnInit } from '@angular/core';
import { ValidationErrors } from "@angular/forms";

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements OnInit {
  @Input() errors: ValidationErrors;
  @Input() errorMessages: { [index: string]: string };
  @Input() hint: string;

  constructor() { }

  ngOnInit() { }
}
