import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "@app/services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private username: string;
  private password: string;
  private title: string = "LoginComponent";

  public form: FormGroup;

  // объект с текстом ошибок
  public fieldMessages = {
    username: {
      hint: "Имя пользователя",
      errors: {
        required: "",
        wrongCredentials: ""
      }
    },
    password: {
      hint: "Пароль",
      errors: {
        required: "",
        wrongCredentials: ""
      }
    }
  };

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      username: [this.username, [
          Validators.required
      ]],
      password: [this.password, [
          Validators.required
      ]]
    })
  }

  public submitForm() {
    this.authService.login(this.form.value).subscribe(result => {
      console.log(`%cAuthService вернул объект:`, 'color: white; font-weight: bold;');
      console.table(result);
      if(result.success) {
        console.log(`redirecting to: %c${this.authService.redirectUrl}`, 'color: orange; font-weight: bold;');
        this.router.navigateByUrl(this.authService.redirectUrl);
      } else {
        this.fieldMessages.username.errors.wrongCredentials = result.errorMessage;
        this.fieldMessages.password.errors.wrongCredentials = result.errorMessage;
      }
    })
  }
}
