import { Component, OnInit } from '@angular/core';
import { Post } from "@app/models/Post";
import { TableService } from "@app/services/table.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  public posts: Post[];
  private title: string = "table component";

  constructor(private tableService: TableService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe((data: { posts: Post[] }) => this.posts = data.posts)
  }
}
