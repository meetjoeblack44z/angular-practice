import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() componentTitle: string;

  public title: string;

  constructor() { }

  ngOnInit() {
    this.title = "header component";
  }
}
