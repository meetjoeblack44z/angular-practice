import { Component, OnInit } from '@angular/core';
import { ArticleService } from "@app/article/services/article.service";
import { Article } from "@app/article/models/Article";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  public article: Article;
  private title: string = `article #${this.route.snapshot.paramMap.get('id')} из article module`;

  constructor(private articleService: ArticleService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe((data: { article: Article }) => {
      this.article = data.article;
    });
  }
}
