import { Component, OnInit } from '@angular/core';
import { ArticleService } from "@app/article/services/article.service";
import { Article } from "@app/article/models/Article";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  private title: string = "articles component из модуля article";
  public articles: Article[];

  constructor(private articleService: ArticleService, private router: ActivatedRoute) { }

  ngOnInit() {
    this.router.data.subscribe((data: { articles: Article[] }) => {
      // мок статьи с некорректной ссылкой
      // нужен для демонстрации работы catchError внутри резолвера
      data.articles.unshift({
        Title: 'Нерабочая ссылка для проверки резолвера',
        Description: 'Описание...',
        PageCount: 2020,
        PublishDate: '121212',
        ID: NaN
      });
      this.articles = data.articles;
    });
  }

}
