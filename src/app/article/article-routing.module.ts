import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticlesComponent } from "@app/article/components/articles/articles.component";
import { ArticleComponent } from "@app/article/components/article/article.component";
import { AuthGuard } from "@app/guards/auth.guard";
import { ArticleResolverService } from "@app/article/services/article-resolver.service";
import { ArticlesResolverService } from "@app/article/services/articles-resolver.service";

const routes: Routes = [
    { path: '', component: ArticlesComponent, canActivate: [AuthGuard], resolve: { articles: ArticlesResolverService } },
    { path: ':id', component: ArticleComponent, canActivate: [AuthGuard], resolve: { article: ArticleResolverService } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ArticleRoutingModule { }
