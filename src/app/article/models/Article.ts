export class Article {
    ID: number;
    Title: string;
    Description: string;
    PageCount: number;
    PublishDate: string;
}