import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Article } from "@app/article/models/Article";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private url: string = "http://deelay.me/3000/https://fakerestapi.azurewebsites.net/api/Books";

  constructor(private http: HttpClient) { }

  public getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.url).pipe(map(articles => {
      articles.splice(8);
      articles.forEach(article => {
        article.Title = article.Title.replace("Book", "Article");
      });
      return articles;
    }));
  }

  public getArticle(id: string): Observable<Article> {
    return this.http.get<Article>(`${this.url}/${id}`);
  }
}
