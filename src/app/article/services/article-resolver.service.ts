import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, Resolve } from "@angular/router";
import { Article } from "@app/article/models/Article";
import { ArticleService } from "@app/article/services/article.service";
import { EMPTY, Observable, of } from "rxjs";
import { catchError, mergeMap, take } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ArticleResolverService implements Resolve<Article> {

  constructor(private articleService: ArticleService, private router: Router) { }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Article> | Observable<never> {
    let articleID = route.paramMap.get("id");

    return this.articleService.getArticle(articleID).pipe(
        take(1),
        mergeMap(article => of(article)),
        catchError(() => {
          this.router.navigate(['not-found']);
          return EMPTY;
        }))
  }
}
