import { Injectable } from '@angular/core';
import { Resolve } from "@angular/router";
import { Article } from "@app/article/models/Article";
import { Observable } from "rxjs";
import { ArticleService } from "@app/article/services/article.service";

@Injectable({
  providedIn: 'root'
})
export class ArticlesResolverService implements Resolve<Article[]> {

  constructor(private articleService: ArticleService) { }

  public resolve(): Observable<Article[]> {
    return this.articleService.getArticles();
  }
}
