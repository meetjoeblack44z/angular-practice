import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { ArticlesComponent } from '@app/article/components/articles/articles.component';
import { ArticleComponent } from '@app/article/components/article/article.component';
import { ArticleRoutingModule } from "@app/article/article-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";

@NgModule({
  declarations: [ArticlesComponent, ArticleComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        ArticleRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatButtonModule
    ],
    exports: [ArticlesComponent]
})

export class ArticleModule { }
