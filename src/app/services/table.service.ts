import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Post } from "@app/models/Post";

@Injectable({
  providedIn: 'root'
})
export class TableService {
  // private postsUrl = "http://deelay.me/5000/https://jsonplaceholder.typicode.com/posts?_limit=10";
  public postsUrl = "http://www.mocky.io/v2/5e256bee2f00006e00ce294d?mocky-delay=5000ms";

  constructor(private http: HttpClient) { }

  public getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.postsUrl, { headers: { "x-user-cache": "15000" }});
  }
}
