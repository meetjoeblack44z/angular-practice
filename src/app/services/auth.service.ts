import { Injectable } from '@angular/core';
import { CookieService } from "ngx-cookie-service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

class LoginResult {
  success: boolean;
  errorMessage: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLoggedIn: boolean = false;
  public redirectUrl: string;
  public currentUser: string;

  private loginResult: LoginResult = {
    success: false,
    errorMessage: null
  };

  // эта апишка возвращает рандомную строку
  // притворимся, что вместо этого токен авторизации нам будет возвращать сервер
  private tokenURL: string = "https://helloacm.com/api/random/?n=128";

  // Map, в котором хранятся сессии авторизованных юзеров.
  // таким образом имитируем хранилище активных сессий на бэке
  // Key:value == Username:Token
  private authorizedUsers: Map<string, string> = new Map();

  // Map, в котором хранятся "существующие" юзеры.
  // таким образом имитируем БД юзеров на бэке
  // Key:value == Username:Password
  private existingUsers: Map<string, string> = new Map([
    ["admin", "123"],
    ["guest", "fbs"]
  ]);

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  public login({username, password}): Observable<LoginResult> {
    // возвращаем observable и отправляем подписчику объект,
    // содержащий результаты авторизации
    return new Observable<LoginResult>(observer => {
      // проверяем, что такой юзер существует и пароль введён правильно
      if(this.existingUsers.has(username) && password === this.existingUsers.get(username)) {
        // получаем авторизационный токен, записываем его в cookies
        // и в Map, в котором хранятся сессии авторизованных юзеров
        this.getToken().subscribe(token => {
          this.authorizedUsers.set(username, token);
          this.cookieService.set("auth-cookie", token);
          console.log(`%ccookie has been set: ${token}`, 'color: orange; font-weight: bold;');

          // формируем объект loginResult, чтобы вернуть его подписчику
          // выставляем соответствующий флаг isLoggedIn
          this.loginResult.success = true;
          this.loginResult.errorMessage = null;
          this.isLoggedIn = true;

          // для упрощения используем паблик переменную
          // с её помощью мы будем узнавать под каким юзером авторизованы
          // и при разлогине удалять из Map`а сессий нужную запись
          this.currentUser = username;

          observer.next(this.loginResult);
          observer.complete();
        });
      } else {
        this.loginResult.success = false;
        this.loginResult.errorMessage = "Имя пользователя или пароль введены неверно";
        this.isLoggedIn = false;

        observer.next(this.loginResult);
        observer.complete();
      }
    })
  }

  public logout(): void {
    this.authorizedUsers.delete(this.currentUser);
    this.isLoggedIn = false;
    this.currentUser = null;
    this.cookieService.delete("auth-cookie")
  }

  // метод для проверки активна ли сессия.
  // если поменять токен сессии в cookies браузера
  // и перейти на защищенный маршрут - должно перекинуть на форму авторизации
  public checkSession(token: string): boolean {
    return token === this.authorizedUsers.get(this.currentUser)
  }

  private getToken(): Observable<string> {
    return this.http.get<string>(this.tokenURL);
  }
}
