import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CacheService {
  constructor() { }

  // метод для сохранения данных в кэш
  public put(url: string, body: string): void {
    localStorage.setItem(url, body);
  }

  // возвращает данные из кэша
  public get(url: string): string  {
    return localStorage.getItem(url);
  }
}
