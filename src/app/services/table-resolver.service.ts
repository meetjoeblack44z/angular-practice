import { Injectable } from '@angular/core';
import { Resolve } from "@angular/router";
import { Post } from "@app/models/Post";
import { TableService } from "./table.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TableResolverService implements Resolve<Post[]>{

  constructor(private tableService: TableService) { }

  public resolve(): Observable<Post[]> {
    return this.tableService.getPosts();
  }
}