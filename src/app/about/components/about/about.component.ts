import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscriber } from "rxjs";
import { Router } from "@angular/router";
import { LogoutDialogComponent } from "@app/about/components/logout-dialog/logout-dialog.component";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit, OnDestroy {
  private title: string = "about component из about.module";

  private idleObservable: Observable<number>;
  private idleTimer: number = 5;
  private idleTimeout: number;
  private idleSubscriber: Subscriber<number>;

  constructor(public dialog: MatDialog, private router: Router) {}

  ngOnInit() {
    this.idleObservable = new Observable<any>(subscriber => {
      this.idleSubscriber = subscriber;
      this.setIdleTimeout();
    });

    this.idleObservable.subscribe(() => this.openDialog());
  }

  ngOnDestroy(): void {
    this.idleSubscriber.unsubscribe();
  }

  private openDialog(): void {
    this.idleSubscriber.unsubscribe();
    const dialogRef = this.dialog.open(LogoutDialogComponent, { width: '500px' });

    dialogRef.afterClosed().subscribe(loggedOut => {
      if(loggedOut) {
        this.router.navigate(['login']);
      } else {
        clearTimeout(this.idleTimeout);
        this.idleObservable.subscribe(() => this.openDialog());
      }
    });
  }

  private setIdleTimeout(): void {
    this.idleTimeout = setTimeout(() => this.idleSubscriber.next(), this.idleTimer * 1000);
  }

  @HostListener('window:mousemove') refreshUserState() {
    clearTimeout(this.idleTimeout);
    this.setIdleTimeout();
  }
}
