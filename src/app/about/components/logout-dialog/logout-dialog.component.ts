import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, Subscription } from "rxjs";
import { scan, takeWhile } from "rxjs/operators";
import { MatDialogRef } from "@angular/material";
import { AuthService } from "@app/services/auth.service";

@Component({
  selector: 'app-logout-dialog',
  templateUrl: './logout-dialog.component.html',
  styleUrls: ['./logout-dialog.component.scss']
})
export class LogoutDialogComponent implements OnInit, OnDestroy {

  private seconds: number = 30;
  private timer: Observable<number> = interval(1000);
  public timerLabel: number = 30;
  private countdownTimer: Observable<number>;
  private countdownSubscriber: Subscription;

  constructor(public dialogRef: MatDialogRef<LogoutDialogComponent>, private authService: AuthService) {}

  ngOnInit(): void {
    this.countdownTimer = this.timer.pipe(
        scan(count => --count, this.seconds),
        takeWhile(val => val >= 0)
    );

    this.countdownSubscriber = this.countdownTimer.subscribe(a => {
      this.timerLabel = a;
      if(a == 0) this.logout();
    });

    // логика на случай если произошел клик вне модального окна или нажат ESC
    this.dialogRef.backdropClick().subscribe(() => this.keepMeIn());
    this.dialogRef.keydownEvents().subscribe(event => event.key === "Escape" ? this.keepMeIn() : '')
  }

  // отписка при уничтожении компонента на случай если будет переход по роутеру
  // через историю браузера
  ngOnDestroy(): void {
    if(this.countdownSubscriber) this.countdownSubscriber.unsubscribe();
  }

  public keepMeIn(): void {
    this.dialogRef.close(false);
  }

  public logout(): void {
    this.authService.logout();
    this.dialogRef.close(true);
  }
}
