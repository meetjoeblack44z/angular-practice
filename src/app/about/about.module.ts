import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from "@app/about/components/about/about.component";
import { AboutRoutingModule } from "@app/about/about-routing.module";

@NgModule({
  declarations: [AboutComponent],
  imports: [
    CommonModule, AboutRoutingModule
  ]
})
export class AboutModule { }
