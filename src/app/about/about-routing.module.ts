import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from "@app/about/components/about/about.component";
import { AuthGuard } from "@app/guards/auth.guard";

const routes: Routes = [
    { path: '', component: AboutComponent, canActivate: [AuthGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AboutRoutingModule { }
