import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from "@app/components/table/table.component";
import { ContentDummyComponent } from "@app/components/content-dummy/content-dummy.component";
import { FormComponent } from "@app/components/form/form.component";
import { LoginComponent } from "@app/components/login/login.component";
import { NotFoundComponent } from "@app/components/not-found/not-found.component";
import { TableResolverService } from "@app/services/table-resolver.service";

const routes: Routes = [
  { path: '', component: ContentDummyComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'table-component', component: TableComponent, resolve: { posts: TableResolverService }},
  { path: 'form-component', component: FormComponent },
  { path: 'articles-component', loadChildren: () => import('@app/article/article.module').then(mod => mod.ArticleModule) },
  { path: 'about-component', loadChildren: () => import('@app/about/about.module').then(mod => mod.AboutModule) },
  { path: 'datafetcher-component', loadChildren: () => import('@app/rxrequests/rxrequests.module').then(mod => mod.RxrequestsModule) },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
