import { NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '@app/app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { CookieService } from "ngx-cookie-service";
import { AppComponent } from '@app/app.component';
import { NavigationComponent } from '@app/components/navigation/navigation.component';
import { HeaderComponent } from '@app/components/header/header.component';
import { FooterComponent } from '@app/components/footer/footer.component';
import { FormComponent } from '@app/components/form/form.component';
import { TableComponent } from '@app/components/table/table.component';
import { ContentDummyComponent } from '@app/components/content-dummy/content-dummy.component';
import { FormFieldComponent } from "@app/components/form-field/form-field.component";
import { LoginComponent } from "@app/components/login/login.component";
import { NotFoundComponent } from '@app/components/not-found/not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from "@angular/material/menu";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatCardModule } from "@angular/material/card";
import { MatTableModule } from "@angular/material/table";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatDialogModule } from "@angular/material/dialog";
import { LogoutDialogComponent } from "@app/about/components/logout-dialog/logout-dialog.component";
import { CachingInterceptorService } from "@app/interceptors/caching-interceptor.service";

registerLocaleData(localeRu);

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HeaderComponent,
    FooterComponent,
    FormComponent,
    TableComponent,
    ContentDummyComponent,
    FormFieldComponent,
    LoginComponent,
    NotFoundComponent,
    LogoutDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatGridListModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule
  ],
  entryComponents: [LogoutDialogComponent],
  providers: [CookieService, { provide: HTTP_INTERCEPTORS, useClass: CachingInterceptorService, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
