export class Planet {
    name: string;
    rotation_period: string;
    orbital_period: string;
    diameter: string;
    climate: string;
    gravity: string;
    terrain: string;
    surface_water: string;
    population: "200000";
    residents: string[];
    films: string[];
    created: Date;
    edited: Date;
    url: string;
}